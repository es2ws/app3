<?php

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on.
|
*/

require_once __DIR__ . '/vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Register our Application
|--------------------------------------------------------------------------
|
| We need to run our PHP Application somehow.
| This bootstraps our basic application and gets it ready for use, then it
| will load up this application.
|
*/

$app = require_once __DIR__ . '/bootstrap/app.php';

if ( $app->isCli() ) {
	$cli_commands = \Console\Commands::getInstance( $app )->init();
} else {
	$web_routes = \Routes\Web::getInstance( $app )->init();
}