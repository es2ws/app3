<?php

namespace App\Controllers;

use Framework\Application;
use Framework\Singleton;

class HomeController extends Singleton
{
	protected static $instance;

	private $app;

	/**
	 * Leaderboard constructor.
	 *
	 * @param Application $app
	 */
	public function __construct( Application $app )
	{
		$this->app = $app;
	}

	/**
	 * Check pooling process action
	 *
	 * @return string
	 */
	public function indexAction(): string
	{
		return $this->app->render( 'index.html', [
			'webSocket' => [
				'host' => $_ENV['WEBSOCKET_HOST'],
				'port' => $_ENV['WEBSOCKET_PORT']
			]
		]
		);
	}
}