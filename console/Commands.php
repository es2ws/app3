<?php

namespace Console;

use Framework\Application;
use Framework\Singleton;

class Commands extends Singleton
{
	protected static $instance;

	private $app;

	protected $arguments;
	protected $commandClassName;

	/**
	 * Commands constructor.
	 *
	 * @param Application $app
	 */
	public function __construct( Application $app)
	{
		$this->app = $app;
		$this->arguments = $_SERVER['argv']; // getting all parameters from console
		array_shift($this->arguments); // removing index.php from arguments
		$this->commandClassName = array_shift( $this->arguments); // get the class that we will execute
	}

	/**
	 * @throws \Exception
	 */
	public function init(){
		$commandClass = $this->app->getCliPath($this->commandClassName);

		if (class_exists($commandClass)) {
			new $commandClass( $this->app, $this->arguments );
		} else{
			throw new \Exception("The class you are looking for: $commandClass was not found");
		}
	}
}