<?php

namespace Routes;

use App\Controllers\HomeController;
use Framework\Application;
use Framework\Singleton;
use Steampixel\Route;

class Web extends Singleton
{
	protected static $instance;

	public $app = null;

	protected $homeController;

	/**
	 * Web constructor.
	 *
	 * @param Application $app
	 */
	public function __construct( Application $app )
	{
		$this->app = $app;

		$this->homeController = HomeController::getInstance( $this->app );
	}

	/**
	 *
	 */
	public function init()
	{
		// Our default route
		Route::add( '/', function(){
			print $this->homeController->indexAction();
		}, 'GET' );

		// Run the router
		Route::run( '/' );
	}
}