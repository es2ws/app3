#Instructions to use the App3

1. Create .env file
    - Insert following:
    ```
        WEBSOCKET_HOST=localhost
        WEBSOCKET_PORT=8080
    ```
2. Run ```composer install```
3. Create virtual host **pokerstars.app3.local**
4. Run **nginx**/**apache** with **php** enabled on root
5. Open **http://pokerstars.app3.local**